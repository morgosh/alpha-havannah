import random

from tabulaRasa.timer import timer
from tabulaRasa.grid import grid
from tabulaRasa.game import game
import math

class node:
	qStrength = 1.5
	cpuct = 1
	maxSearch = 20
	def __init__(self, grid, onTurn, parentNode, childNodes):
		self.grid = grid
		self.position = None
		self.onTurn = onTurn
		self.notOnTurn = 2 if onTurn == 1 else 1
		self.end = False
		self.N = 0 #Number of times entered
		self.W = 0 #Total value of next state
		self.P = None #prior value, move probability
		self.Q = None #average value
		self.v = None#value for picking this position, decided by NN
		self.mustPlay = None
		
		#self.Policy = None we only need this if we dont generate next nodes

		self.definiteWin = False
		self.definiteLoss = False
		self.parentNode = parentNode
		self.childNodes = childNodes
		if(parentNode is not None):
			parentNode.childNodes.append(self)

	def chooseBestNode(self):
		#Tabula rasa picks the most searched one, there might be some edge cases but most of the time the most searched one proved to be the best!
		#For low mcts played games, W might be better to use
		childNodes = self.childNodes
		self.childNodes.sort(key=lambda x: x.W, reverse=True)
		if(not self.mustPlay == None):
			for childNode in self.childNodes:
				if(childNode.position == self.mustPlay):
					return childNode
		if(False):
			bestNode = self.childNodes[1] if (len(self.childNodes) > 1 and self.childNodes[1].Q and self.childNodes[1].Q > self.childNodes[0].Q) else self.childNodes[0]
		else:
			bestNode = self.childNodes[0]
		return bestNode

	def chooseNodeStochaistic(self):
		self.childNodes.sort(key=lambda x: x.N, reverse=True)
		rando = random.uniform(0, self.N)
		currentNumber = 0
		for childNode in self.childNodes:
			currentNumber += childNode.N
			if(currentNumber >= rando):
				return childNode
		print("did not find")
		return self.childNodes[0]

		return chosenNode
	def chooseNextNode(self, evaluationAlgorithm):
		childNodes = self.childNodes
		bestValue = node.cpuct * self.childNodes[0].P  * (math.sqrt( self.N ) / (1 + self.childNodes[0].N))
		bestNode = self.childNodes[0]
		
		maxN = self.childNodes[0].N
		maxNNode = self.childNodes[0]
		#for childNode in childNodes:
		#	if(childNode.definiteLoss == False and childNode.N > 0 and childNode.N < 3):
		#		return childNode
		if(not self.mustPlay == None):
			for childNode in self.childNodes:
				if(childNode.position == self.mustPlay):
					return childNode
		#Preveri če je prezahtevno, načeloma lejko znižaš ka ne preverja vseh...
		#Todo to je naj samo pri competitive lekar, ker ovak se ne fči...
		count = min(len(childNodes), node.maxSearch) if evaluationAlgorithm.agentName != "random" else len(childNodes)
		for i in range(count):
			childNode = childNodes[i]
			if(childNode.definiteLoss == False):
				U = node.cpuct * childNode.P  * (math.sqrt( self.N ) / (1 + childNode.N))
				equationQ = childNode.Q if not childNode.Q == None else 0.5#1 - self.Q  #Q #The mean value
				nodeValue = node.qStrength*equationQ + U
				if(nodeValue > bestValue):
					bestValue = nodeValue
					bestNode = childNode
		return bestNode


	def backPropagation(self, v, toPlay):
		self.N += 1
		self.W += 1-v if self.onTurn == toPlay else v
		self.Q = self.W / self.N
		if(self.Q > 1):
			self.v = 1
			print("printing")
			print(v)
			print(self.W)
			print(self.N)
		if(self.parentNode is not None):
			self.parentNode.backPropagation(v, toPlay)

	def sortChildren(self, by = "N"):
		if(by == "N"):
			self.childNodes.sort(key=lambda x: x.N, reverse=True)
		elif(by == "P"):
			self.childNodes.sort(key=lambda x: x.P, reverse=True)
		for childNode in self.childNodes:
			childNode.sortChildren(by)

	def sortParents(self):
		if(self.parentNode is not None):
			self.parentNode.childNodes.sort(key=lambda x: x.N, reverse=True)
			self.parentNode.sortParents()

	def generateChildNodes(self, policy):
		for move in self.grid.emptyList:
				childNode = node(None, self.notOnTurn, self, [])#grid.deepCopy(self.grid)
				childNode.position = move
				#childNode.grid.play(move, self.onTurn)
				childNode.P = policy[move[0]][move[1]]
				#if(childNode.grid.checkVictory(move)):
					# to po MCTS reworki ma kakši smisel sploj ka childNode brišeš?
					#set only one viable move as it is always a win
					#TODO is it ok that we set only one viable play?
					#TODO why are other moves potentialy losses? and why does this only worth 1 play? for example other moves are potentially also wins, but we skip them.
					#self.childNodes = [childNode]
				#	childNode.definiteWin = True
				#	childNode.parentNode.definiteLoss = True
					#can we put this? N number will be incorrect.
				#	childNode.backPropagation(1,childNode.onTurn)
					#return

	def print(rootNode):
		print("this node total " + str(rootNode.W) + "/" + str(rootNode.N))
		for childNode in rootNode.childNodes:
			print("x:" + str(childNode.position[0]) + " y:" + str(childNode.position[1]) + " " + str(childNode.W) + "/" + str(childNode.N))

	def printPrediction(rootNode):
		if(len(rootNode.childNodes) > 0):
			print(str(rootNode.onTurn) + " plays-> x:" + str(rootNode.childNodes[0].position[0]) + " y:" + str(rootNode.childNodes[0].position[1]) + " " + str(rootNode.childNodes[0].W) + "/" + str(rootNode.childNodes[0].N))
			node.printPrediction(rootNode.childNodes[0])

	def checkVictory(self):
		return self.grid.checkVictory(self.position)