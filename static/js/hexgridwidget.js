/*global $, document*/
$.fn.hexGridWidget = function (size, grid, columns, rows, cssClass) {
	'use strict';
	var createSVG = function (tag) {
		return $(document.createElementNS('http://www.w3.org/2000/svg', tag || 'svg'));
	};
	return $(this).each(function () {
		var element = $(this),
			hexClick = function () {
				var hex = $(this);
				element.trigger($.Event('hexclick', hex.data()));
			},
			svgParent = createSVG('svg').attr('tabindex', 1).appendTo(element).attr({
			id: "mysvg"}).css({
				width: (2.2 * columns + 2) * size,
				height: (1.6 * rows + 1) * size
			}),
			column, row, center,
			toPoint = function (dx, dy) {
				return Math.round(dx + center.x) + ',' + Math.round(dy + center.y);
			};
		for (row = 0; row < rows; row++) {
			for (column = 0; column < columns; column++) {
				if (grid[column][row] != null) { 
					var padding = row * size;
					var x = Math.round((column) * size * 2) + padding;
					center = {x: x , y: Math.round(1.5 * size * (row)) + size * 2 };

					//var myGroup = createSVG('g').appendTo(svgParent).attr({x: center.x, y: center.y});

					var cssClassColor = cssClass;
					if (grid[column][row] == 1) {
						cssClassColor += " red"
					} else if (grid[column][row] == 2) {
						cssClassColor += " blue"
					} else  {
						cssClassColor += " white"
					}
					var id = "hex" + column + "" + row;

					var myPoly = createSVG('polygon').attr({
						points: [
							toPoint(0, -size),
							toPoint(size, -size / 2),
							toPoint(size, +size / 2),
							toPoint(0, size),
							toPoint(-size, +size / 2),
							toPoint(-size, -size / 2),
						].join(' '),
						'id': id,
						'class': cssClassColor,
						tabindex: 1
					})
						.appendTo(svgParent).data({ center: center, row: row, column: column }).on('click', hexClick).attr({ 'hex-row': row, 'hex-column': column });

					var txt = document.createTextNode(column + "," + row);
					var myText = document.createElementNS('http://www.w3.org/2000/svg', "text");
					myText.appendChild(txt);
					$(myText).attr({ x: center.x - 0.40 * size, y: center.y, "font-size": 10 }).appendTo(svgParent).data({ center: center, row: row, column: column });
				}
			}
		}
	});
};