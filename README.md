in 1/10/2020 tensorflow supports python 3.8.X versions, not yet 3.9

pip install flask
pip install numpy
pip install tensorflow

#1
py app.py to run demo

#2
py runPingPongTraining.py to run recursive 3 step training, 1 => getting data 2 -> training new NN  3 -> repeat with newer version

#3
py runTests.py to run tests

#4
py runTNN.py to train NN on specific data


To view tensorboard log:
tensorboard --logdir <path>


Licensed under CC BY 4.0