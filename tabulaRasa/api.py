

from tabulaRasa.neuralNetwork import neuralNetwork
from tabulaRasa.node import node
from tabulaRasa.grid import grid
from tabulaRasa.game import game
from tabulaRasa.tabulaRasa import ai

class api:
	
	gridRadius = 4
	mctsIterations = 10 # note that 10 is very low, for competitive put atleast 128

	# actions
	def initNetwork():
		if(not neuralNetwork.NNset()):
			neuralNetwork.globalNetwork = neuralNetwork("pingPongAgent")

	def actionNewGame(radius):
		api.initNetwork()
		gameX = game(radius)
		rootNode = node(grid(gameX), 1, None, [])
		rootNode.grid.playRandom(2)

		#from threading import Thread
		#Thread(target=ai.monteCarlo, args=[rootNode, neuralNetwork.globalNetwork, 1234567]).start()

		return rootNode.grid
	def actionPlay(x, y, gridTMP):
		gameX = game(api.gridRadius)
		grids = grid.formatGridToDouble(gridTMP, gameX.gridSize)
		gridX = grid(gameX, grids)
		
		# checkLegal
		if(gridX.checkLegal([x,y], 1)):
			gridX.play([x,y], 1)
			if gridX.checkVictory([x,y]):
				return {
					'legal': True,
					'play': [None,None],
					'winner': 1
				}
			else:
				nodeX = node(gridX, 2, None, [])
				ai.monteCarlo(nodeX, neuralNetwork.globalNetwork, api.mctsIterations)
				bestNode = nodeX.chooseBestNode()
				play = bestNode.position
				gridX.play(play, 2)
				return {
					'legal': True,
					'play': play,
					'grid': grid.formatGridToSingle(gridX.grids, gameX.gridSize),
					'winner': 2 if gridX.checkVictory(play) else 0
				}
		return { 'legal': False }

	def actionGetNode(pathCurrent):
		FeList = []
		localCounter = 0
		currentNode = node.globalNode
		for index in pathCurrent:
			indexInt = int(index)
			currentNode = currentNode.childNodes[indexInt]
		for childNode in currentNode.childNodes:
			path = pathCurrent.copy()
			path.append(localCounter)
			FeList.append({
				"id": "node"+ str(tabulaRasa.myGlobalFENodeCounter) + "-"+ tabulaRasa.randomString(8),   #TODO remove random if everything ok with counter
				"name": "<br>x:" +  str(childNode.position[0]) + " y:" +  str(childNode.position[1]) + " <br> N: " + str(round(childNode.N, 2)) +"<br> P: " + str(round(childNode.P, 2))+"<br> Q:" + str(round(childNode.Q, 3) if not childNode.Q == None else 0.5)+ "<br> v:" + str(round(childNode.v, 3) if childNode.v is not None else "n"),
				"data": {
					"path": path,
				}
			})
			localCounter+=1
			tabulaRasa.myGlobalFENodeCounter+=1
		return FeList

	def actionTest(testName = 0):
		if(testName == 1):
			return testing.testMy()
		return testing.test()

	def actionTestNeuralNetwork(testName = 0):
		return neuralNetwork.test()