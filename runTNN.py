#run this if you want to train neural network from specific training data
import logging

from tabulaRasa.neuralNetwork import neuralNetwork


logger = logging.getLogger('root')
FORMAT = "[%(filename)s:%(lineno)s - %(levelname)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(format=FORMAT, level=logging.NOTSET)

import tensorflow as tf
tf.get_logger().setLevel('INFO')#TODO

neuralNetwork.createNetwork("pingPongDataCycle", "pingPongAgent")


#neuralNetwork.createNetwork(trainingData, loadModel)
#neuralNetwork.createSeperateResidualNetwork(trainingData)
#neuralNetwork.createNonResidual(trainingData, loadModel)