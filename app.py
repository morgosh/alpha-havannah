
"""
Created 2020

@author: Nino Serec
"""

#imports
from flask import Flask, render_template, json, request, session, redirect
from tabulaRasa.api import api

#initialize the flask and SQL Objects
app = Flask(__name__)

#define methods for routes (what to do and display)
@app.route("/")
def main():
    return render_template('index.html')

@app.route("/main")
def return_main():
    return render_template('index.html')

#sectionsLong
@app.route("/sectionRules")
def return_sectionRules():
    return render_template('section/rules.html')


@app.route("/test")
def return_test():
    return render_template('test.html')

@app.route("/test2")
def return_test2():
    return render_template('test2.html')

#
#
#api
@app.route('/runTests')
def runTests():
	return json.dumps(api.testing.test())

@app.route('/init_map')
def init_map():
    radius = int(request.args.get('radius'))
    grid = api.actionNewGame(radius)

    combinedGrid = []
    size = radius*2 + 1
    for i in range(size):
        combinedGrid.append([])
        for j in range(size):
            if(grid.gridP1[i][j] == None):
                combinedGrid[i].append(None)
            elif(grid.gridP1[i][j] == 1):
                combinedGrid[i].append(1)
            elif(grid.gridP2[i][j] == 1):
                combinedGrid[i].append(2)
            else:
                combinedGrid[i].append(0)
    return json.dumps(combinedGrid)

@app.route('/play', methods=['POST'])
def play():
    request.get_json()
    x = int(request.form["x"])
    y = int(request.form["y"])
    grid = json.loads(request.form["grid"])
    response = api.actionPlay(x, y, grid)
    return json.dumps(response)

@app.route('/getNode', methods = ['POST'])
def getNode():
	#we send the whole path
	request.get_json()
	if not request.form:
		path = []
	else:
		path = request.form.getlist("path[]")
	nodeChildren = api.actionGetNode(path)
	return json.dumps(nodeChildren)

if __name__ == "__main__":
    #app.run(host='0.0.0.0', port='5000')
    print("it takes some time for NN model to initialize...")
    api.initNetwork()
    app.run(debug=True)
